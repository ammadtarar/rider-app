const Notifications = require("../controllers/Notifications.js");

import axios from "axios";
// const baseURL = "http://localhost:3003/";
// const baseURL = "https://api.nxgen.internal.sawatechnologies.org/";
const baseURL = "https://api.nxgenlabs.com.pk/";

axios.defaults.baseURL = baseURL;

const HTTP = axios.create({
    baseURL: process.env.VUE_APP_API,
    // responseType: "json",http://api.nxgenlabs.com.pk:16992/api/create-user
});

HTTP.interceptors.request.use(
    function (config) {
        Notifications.showLoading();
        const token = localStorage.getItem("rider_token");
        if (token) {
            config.headers["Authorization"] = `${token}`;
        }
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

HTTP.interceptors.response.use(
    (response) => {
        Notifications.hideLoading();
        return response;
    },
    (error) => {
        Notifications.hideLoading();
        if (error.response && error.response.data) {
            Notifications.showNotification("danger", error.response.data.message);
        } else {
            Notifications.showNotification("danger", error.message);
        }

        // if (
        //     error.response.config.url !== "api/authenticate" &&
        //     error.response.status === 401
        // ) {
        //     localStorage.removeItem("roles");
        //     localStorage.removeItem("id_token");
        //     location.reload();
        //     alert("Session timeout");
        //     return;
        // }
        throw error;
    }
);

const URLS = {
    baseURL: baseURL,
    AUTH: {
        LOGIN: "rider/login"
    },
    APPOINTMENT: {
        LIST: "appointment/list"
    }

};

export { HTTP, URLS };
