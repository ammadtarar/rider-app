const Notifications = require("../controllers/Notifications.js");
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Login",
    component: () =>
      import("../views/Login.vue")
  },
  {
    path: "/home",
    name: "Home",
    meta: { protected: true },
    component: () =>
      import("../views/Home.vue")
  },
  {
    path: "/detail",
    name: "Detail",
    meta: { protected: true },
    component: () =>
      import("../views/Detail.vue")
  },

];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.protected)) {
    if (!localStorage.getItem('rider_token')) {
      console.log('yup');
      Notifications.showNotification("danger", "Please login before continuing...");
      next({
        path: '/',
        params: { nextUrl: to.fullPath }
      });
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router;
